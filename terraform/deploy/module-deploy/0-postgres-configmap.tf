/*====
postgres-configmap.yaml
====*/

resource "kubectl_manifest" "postgres_configmap_0" { 
yaml_body = <<YAML
apiVersion: v1
kind: ConfigMap
metadata:
  name: postgres-config
  namespace: desafio
  labels:
    app: postgres
data:
  POSTGRES_DB: metabase
  POSTGRES_USER: admin
  POSTGRES_PASSWORD: test123
YAML
}

