/*====
postgres-service.yaml
====*/

resource "kubectl_manifest" "postgres_service_3" {
yaml_body = <<YAML
apiVersion: v1
kind: Service
metadata:
  name: postgres
  namespace: desafio
  labels:
    app: postgres
spec:
  type: NodePort
  ports:
   - port: 5432
  selector:
   app: postgres
YAML
depends_on = [
  kubectl_manifest.postgres_deployment_2_1
]
}
