/*====
1-postgres-pv.yaml
====*/

resource "kubectl_manifest" "postgres_pv_1" {
yaml_body = <<YAML
kind: PersistentVolume
apiVersion: v1
metadata:
  name: postgres-pv-volume
  namespace: desafio
  labels:
    type: local
    app: postgres
spec:
  storageClassName: manual
  capacity:
    storage: 5Gi
  accessModes:
    - ReadWriteMany
  hostPath:
    path: "/mnt/data"
YAML
depends_on = [
  kubectl_manifest.postgres_configmap_0
]
}



