/*====
metabase-deployment.yaml
====*/

resource "kubectl_manifest" "metabase_deployment_5" {
yaml_body = <<YAML
apiVersion: apps/v1
kind: Deployment
metadata:
  name: metabase
  namespace: desafio
  labels:
    name: metabase
spec:
  replicas: 1
  selector:
    matchLabels:
      name: metabase
  template:
    metadata:
      labels:
        name: metabase
    spec:
      containers:
      - name: metabase
        image: metabase/metabase:latest
        imagePullPolicy: "IfNotPresent"
        envFrom:
        - configMapRef:
            name: metabase-config
YAML
depends_on = [
  kubectl_manifest.metabase_configmap_4
]
}


