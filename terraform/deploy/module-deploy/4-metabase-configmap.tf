/*====
metabase-configmap.yaml
====*/

resource "kubectl_manifest" "metabase_configmap_4" {
yaml_body = <<YAML
apiVersion: v1
kind: ConfigMap
metadata:
  name: metabase-config
  namespace: desafio
data:
  MB_DB_TYPE: postgres
  MB_DB_DBNAME: metabase
  MB_DB_PORT: "5432"
  MB_DB_USER: admin
  MB_DB_PASS: test123
  MB_DB_HOST: postgres
YAML
depends_on = [
  kubectl_manifest.postgres_service_3
]
}
