/*====
2-postgres-pvc.yaml
====*/

resource "kubectl_manifest" "postgres_pvc_2" {
yaml_body = <<YAML
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: postgres-pv-claim
  labels:
    app: postgres
  namespace: desafio
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 5Gi
YAML
depends_on = [
  kubectl_manifest.postgres_pv_1
]
}



