/*====
metabase-service.yaml
====*/

resource "kubectl_manifest" "metabase_service_6" {
yaml_body = <<YAML
apiVersion: v1
kind: Service
metadata:
  name: metabase
  namespace: desafio
spec:
  selector:
    name: metabase
  ports:
  - port: 3000
    targetPort: 3000
    nodePort: 30892
  type: NodePort
YAML
depends_on = [
  kubectl_manifest.metabase_deployment_5
]
}

