/*====
postgres-deployment.yaml
====*/

resource "kubectl_manifest" "postgres_deployment_2_1" {
yaml_body = <<YAML
apiVersion: apps/v1
kind: Deployment
metadata:
  name: postgres
  namespace: desafio
spec:
  replicas: 1
  selector:
    matchLabels:
      app: postgres
  template:
    metadata:
      annotations:
        sidecar.istio.io/inject: "false"
      labels:
        app: postgres
    spec:
      containers:
        - name: postgres
          image: postgres:10.1
          imagePullPolicy: "IfNotPresent"
          ports:
            - containerPort: 5432
          envFrom:
            - configMapRef:
                name: postgres-config
          volumeMounts:
            - mountPath: /var/lib/postgresql/data
              name: postgredb
      volumes:
        - name: postgredb
          persistentVolumeClaim:
            claimName: postgres-pv-claim
YAML
depends_on = [
  kubectl_manifest.postgres_pvc_2
]
}
