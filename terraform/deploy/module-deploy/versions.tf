/*====
DEPENDENCE from K8S
====*/
terraform {
   required_providers {
    kubectl = {
      source = "gavinbunney/kubectl"
      version = "1.11.2"
    }
  }
  required_version = ">= 0.15"
}
