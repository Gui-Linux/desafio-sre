/*====
File with providers
====*/

provider "aws" {
  region  = "us-east-1"
}

terraform {
  required_providers {
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "1.11.2"
    }
  }
}

provider "kubernetes" {
  config_path = "kube.config"
}

provider "helm" {
  provider "kubernetes" {
  config_path = "kube.config"
  }
}

provider "kubectl" {
  config_path = "kube.config"
}