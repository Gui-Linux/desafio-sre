provider "aws" {
  profile = "pessoal"
  version = "~> 4.0"
  region = "us-east-1"
}

terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket         = "desafio-sre"
    key            = "desafio-sre.tfsate"
    region         = "us-east-1"
    profile = "pessoal"
  }
}