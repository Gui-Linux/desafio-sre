/*====
File from main Project
====*/

module "ec2" {
  source            = "./module-ec2"
  master-name       = "RANCHER-K8S-MASTER"
  ami               = "ami-052efd3df9dad4825"
  instance-type     = "t2.medium"
  key-name          = "ansible-class"
  availability-zone = "us-east-1c"
  security-groups   = ["sg-040e107f01a95d2b3"]
  subnet-id         = "subnet-68649025"
  environment       = "prod"
}

module "tg" {
  source            = "./module-tg"
  alb-name          = "lb-rancher-k8ss"
  external-subnet   = ["subnet-adb59683,subnet-68649025"]

  tg-name           = "tg-rancher-k8ss"
  vpc-id            = "vpc-2652225c"
  SGs               = ["sg-040e107f01a95d2b3"]
  instance-ip       = module.ec2.instance_id

  depends_on = [
    module.ec2
  ]
}