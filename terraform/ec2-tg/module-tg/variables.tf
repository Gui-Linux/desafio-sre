/*====
File with Variables from Loadbalancer 
====*/

variable "alb-name" {
}

variable "internet-facing" {
  default = "false"
}

variable "external-subnet" {

}


/*====
Target Group
====*/


variable "tg-name" {
}

variable "vpc-id" {
}

variable "SGs" {

}

variable "instance-ip" {

}

