resource "aws_lb" "loadbalancer" {
  name                = "${var.alb-name}"
  internal            = "${var.internet-facing}" 
  subnets             = "${var.external-subnet}"
  security_groups     = "${var.SGs}" 

  tags = {
    Name        = "${var.alb-name}"
  }
}


resource "aws_lb_target_group" "lb_target_group" {
  name        = "${var.tg-name}"
  port        = "80"
  protocol    = "HTTP"
  vpc_id      = "${var.vpc-id}"
  target_type = "instance"


  health_check {
    healthy_threshold   = "5"
    interval            = "30"
    protocol            = "HTTP"
    unhealthy_threshold = "2"
  }
}


resource "aws_lb_listener" "lb_listener" {
  default_action {
    target_group_arn = "${aws_lb_target_group.lb_target_group.id}"
    type             = "forward"
  }

  load_balancer_arn = "${aws_lb.loadbalancer.arn}"
  port              = "80"
  protocol          = "HTTP"
}

resource "aws_lb_target_group_attachment" "metabase-tg-attachment" {
  target_group_arn = "${aws_lb_target_group.lb_target_group.arn}"
  target_id        = "${var.instance-ip}"  
  port             = 30892
}
