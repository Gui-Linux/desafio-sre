/*====
File with Variables from Kubernets Instance 
====*/

variable "master-name" {
}

variable "ami" {
}

variable "instance-type" {
}

variable "key-name" {
}

variable "availability-zone" {
}

variable "security-groups" {

}

variable "subnet-id" {
}


/*====
tags
====*/ 

variable "terraform" {
  default = "sim"
}

variable "type" {
  default = "master"
}

variable "environment" {
}


