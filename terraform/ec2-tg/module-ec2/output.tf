output "instance_id" {
  value       = aws_instance.k8s-Rancher.id
  description = "The private IP address of the main server instance."
  
  depends_on = [
    aws_instance.k8s-Rancher
  ]
}