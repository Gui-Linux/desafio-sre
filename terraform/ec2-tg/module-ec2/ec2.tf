# Criação da instancia - Master
resource "aws_instance" "k8s-Rancher" {
  ami                         = "${var.ami}"
  instance_type               = "${var.instance-type}"
  key_name                    = "${var.key-name}"
  disable_api_termination     = "true"
  ebs_optimized               = "false"
  availability_zone           = "${var.availability-zone}"
  associate_public_ip_address = "false"
  vpc_security_group_ids      = "${var.security-groups}"
  subnet_id                   = "${var.subnet-id}"


  tags = {
      Name = "${var.master-name}"
      Terraform = "${var.terraform}"
      Type = "${var.type}"
      Environment = "${var.environment}"
  }
}