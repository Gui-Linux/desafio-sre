# Desafio SRE

Neste repositório contém todos os scripts que foram utilizados para instânciar toda a infra e fazer o deplor do projeto.


## Ferramentas

As ferramentas utilizadas foram:

* Rancher \
A utilização do `Rancher` facilita muito a implantação de um cluster Kubernetes, foi essencial para garantir a alta disponibilidade dos apps.

* Ansible \
Foi utilizado também o `Ansible` para fazer o deploy do cluster RKE( Rancher ).

* Terrafrom \
Para criar os recursos na `AWS` foi utilizado o `Terraform` (ALB, Target-Group, EC2), e também para deployar os apps no kubernetes.

* Jenkins \
Com ele criei dois steps para iniciar a receita do terraform e fazer o deploy em seguida (foi configurado no job para clonar, não está no script).


## Desafios

Foi uma ótima oportunidade para aprender e executar algumas coisas que aprendi.. no entanto, tive alguns problemas para criar um HPA por requests... tentei executar o istio e utilizar da métrica "istio_requests_total", mas não consegui concluir. 

Testar os `.tfs` e as `playbooks` também rs

